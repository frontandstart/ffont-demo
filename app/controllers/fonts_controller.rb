class FontsController < InheritedResources::Base

  private

    def font_params
      params.require(:font).permit(:name, :status)
    end
end

