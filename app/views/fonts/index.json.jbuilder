json.array!(@fonts) do |font|
  json.extract! font, :id, :name, :status
  json.url font_url(font, format: :json)
end
