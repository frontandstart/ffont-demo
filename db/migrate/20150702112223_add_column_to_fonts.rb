class AddColumnToFonts < ActiveRecord::Migration
  def change
    add_column :fonts, :category, :string
    add_column :fonts, :lang, :string
  end
end
