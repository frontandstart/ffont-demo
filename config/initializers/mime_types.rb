# Be sure to restart your server when you modify this file.

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf

Mime::Type.register "application/vnd.ms-fontobject",  :eot
Mime::Type.register "application/svg+xml",            :svg
Mime::Type.register "application/x-font-ttf",         :ttf
Mime::Type.register "font/opentype",                  :otf
Mime::Type.register "application/x-font-woff",        :woff
Mime::Type.register "application/x-font-woff2",       :woff2